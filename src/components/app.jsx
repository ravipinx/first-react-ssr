import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { FaArrowRight } from 'react-icons/fa';

import Table from '../sharedComponents/Table';
import LoadingOrError from '../sharedComponents/LoadingOrError';

import HistoricStationsData from './historic-stations.json';

class App extends React.Component {
	constructor(props) {
		super(props);
		this.loadData = this.loadData.bind(this);

		this.state = {
			mostHistoricStations: HistoricStationsData.slice(0, 10)
		};
	}

	loadData = () => {
		console.log('loadData');
		this.setState({
			mostHistoricStations: HistoricStationsData.slice(
				0,
				this.state.mostHistoricStations.length + 10
			)
		});
	};

	render() {
		const tableHead = [
			[
				{
					style: {
						fontSize: '13px',
						padding: '16px 8px'
					},
					value: 'System ID'
				},
				{
					style: {
						fontSize: '13px',
						padding: '16px 8px'
					},
					value: 'Name'
				},
				{
					style: {
						fontSize: '13px',
						padding: '16px 8px'
					},
					value: 'Location'
				},
				{
					style: {
						compress: true,
						fontSize: '13px',
						padding: '16px 8px'
					},
					value: 'Country Code'
				},
				{
					style: {
						compress: true,
						fontSize: '13px',
						padding: '16px 8px'
					},
					value: 'URL'
				}
			]
		];

		const tableRows = this.state.mostHistoricStations.map((v) => {
			return [
				{
					onClick: () => alert(`${v.system_ID}`),
					style: {
						bold: true,
						button: true,
						hover: true,
						fontSize: '13px'
					},
					value: (
						<span>
							{v.system_ID || '--'}
							{v.system_ID && <StyledFaArrowRight />}
						</span>
					)
				},
				{
					style: {
						fontSize: '13px'
					},
					value: <span>{v.name || '-'}</span>
				},
				{
					style: {
						fontSize: '13px'
					},
					value: <span>{v.location || '-'}</span>
				},
				{
					style: {
						bold: true,
						fontSize: '13px'
					},
					value: <span>{v.country_code || '-'}</span>
				},
				{
					style: {
						fontSize: '13px'
					},
					value: <span>{v.URL || '-'}</span>
				}
			];
		});

		const tableStruct = {
			tableHead: tableHead,
			tableRows: tableRows
		};
		return (
			<MainContainer>
				{this.state.mostHistoricStations.length > 0 ? (
					<Table
						data={this.state.mostHistoricStations}
						loadData={this.loadData}
						maxSize={
							(HistoricStationsData && parseInt(HistoricStationsData.length)) ||
							0
						}
						table={tableStruct}
					/>
				) : (
					<LoadingContainer>
						<LoadingOrError
							loading={true}
							loadingMsg={'Fetching Most Historic Stations...'}
							err={false}
							errMsg={''}
						/>
					</LoadingContainer>
				)}
			</MainContainer>
		);
	}
}

const MainContainer = styled.div`
	margin: 30px 0px;
`;

const LoadingContainer = styled.div`
	background-color: #ffffff;
	border: 1px solid #ececec;
	border-radius: 4px;
	display: flex;
	height: 100%;
	min-height: 400px;
	padding: 24px;
	justify-content: center;
	align-items: center;
`;
const StyledFaArrowRight = styled(FaArrowRight)`
	font-size: 10px;
	margin-left: 8px;
	margin-bottom: 2px;
`;

export default App;
